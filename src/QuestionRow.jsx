import { useRef } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import React from 'react';
import { Field } from 'react-final-form';
import { TextField, Radio } from 'final-form-material-ui';
import { Grid, RadioGroup, FormLabel, FormControl, FormControlLabel } from '@material-ui/core';
import { Menu, DeleteForever } from '@material-ui/icons';

export default function QuestionRow({ id, index, question, questions, moveQuestion, editFormMode }) {
  const ref = useRef(null);
  const [{ handlerId }, drop] = useDrop({
    accept: 'question',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }
      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect();
      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      // Determine mouse position
      const clientOffset = monitor.getClientOffset();
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      moveQuestion(dragIndex, hoverIndex);
      item.index = hoverIndex;
    },
  });
  const [{ isDragging }, drag] = useDrag({
    type: 'question',
    item: () => {
      return { id, index };
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
    canDrag: editFormMode,
  });

  drag(drop(ref));

  return (
    <Grid
      item
      spacing={1}
      container
      alignItems="flex-end"
      key={`question-${index}`}
      justifyContent="space-between"
      ref={ref}
      data-handler-id={handlerId}
    >
      {['boolean', 'multiple'].indexOf(question.type) > -1 ? (
        <Grid item>
          <FormControl component="fieldset">
            <FormLabel component="legend">{question.question}</FormLabel>
            <RadioGroup row>
              {question.options.map((option) => (
                <FormControlLabel
                  label={option}
                  control={<Field name={`question_${index}`} component={Radio} type="radio" value={option} />}
                />
              ))}
            </RadioGroup>
          </FormControl>
        </Grid>
      ) : (
        <Grid item xs={editFormMode ? 10 : 12}>
          <Field
            fullWidth
            name={`question_${index}`}
            component={TextField}
            type={question.type === 'number' ? 'number' : 'text'}
            label={question.question}
          />
        </Grid>
      )}
      {editFormMode && (
        <Grid container item xs={2} justifyContent="space-between">
          {questions.length > 1 ? (
            <DeleteForever
              style={{ cursor: 'pointer' }}
              onClick={() => {
                const newQuestions = [...questions];
                newQuestions.splice(i, 1);
                setQuestions(newQuestions);
                form.change(`question_${index}`, undefined);
              }}
            />
          ) : (
            <div></div>
          )}
          <Menu style={{ cursor: 'hand' }} />
        </Grid>
      )}
    </Grid>
  );
}
