import React from 'react';
import { Form, Field } from 'react-final-form';
import { TextField, Select } from 'final-form-material-ui';
import { Grid, Card, Button, MenuItem } from '@material-ui/core';

export default function AddQuestionForm({ questions, setQuestions }) {
  return (
    <Form
      onSubmit={(values) => {
        let newQuestion = {
          type: values.new_question_type,
          question: values.new_question,
        };
        if (values.new_question_type === 'boolean') {
          newQuestion.options = [values.boolean_option_1, values.boolean_option_2];
        } else if (values.new_question_type === 'multiple') {
          newQuestion.options = [];
          [...Array(6)].map(
            (x, i) =>
              values[`multiple_option_${i + 1}`] && newQuestion.options.push(values[`multiple_option_${i + 1}`]),
          );
        }
        setQuestions(questions.concat([newQuestion]));
      }}
      initialValues={{
        new_question_type: 'text',
      }}
      validate={(values) => {
        const errors = {};
        if (!values.new_question) errors.new_question = 'Required';

        if (values.new_question_type === 'boolean') {
          if (!values.boolean_option_1) errors.boolean_option_1 = 'Required';
          if (!values.boolean_option_2) errors.boolean_option_2 = 'Required';
        }

        if (values.new_question_type === 'multiple') {
          if (!values.multiple_option_1) errors.multiple_option_1 = 'Required';
          if (!values.multiple_option_2) errors.multiple_option_2 = 'Required';
          if (!values.multiple_option_3) errors.multiple_option_3 = 'Required';
        }
        return errors;
      }}
      render={({ handleSubmit, reset, submitting, pristine, values }) => (
        <form onSubmit={handleSubmit} noValidate>
          <h3>Add Question to Form Above</h3>
          <Card style={{ padding: 16 }} variant="outlined" square>
            <Grid container spacing={2} alignItems="flex-end">
              <Grid item xs={12} style={{ paddingTop: 20 }}>
                <Field
                  fullWidth
                  name="new_question_type"
                  component={Select}
                  label="Question Type"
                  formControlProps={{ fullWidth: true }}
                >
                  <MenuItem value="text">Text</MenuItem>
                  <MenuItem value="boolean">Boolean</MenuItem>
                  <MenuItem value="number">Number</MenuItem>
                  <MenuItem value="multiple">Multiple Choice</MenuItem>
                </Field>
              </Grid>
              <Grid item xs={12}>
                <Field
                  fullWidth
                  required
                  name="new_question"
                  component={TextField}
                  type="text"
                  label='Question e.g. "What is your favorite color?"'
                />
              </Grid>
              {values.new_question_type === 'boolean' ? (
                <>
                  <Grid item xs={6}>
                    <Field
                      fullWidth
                      required
                      name="boolean_option_1"
                      component={TextField}
                      type="text"
                      label="Option 1"
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <Field
                      fullWidth
                      required
                      name="boolean_option_2"
                      component={TextField}
                      type="text"
                      label="Option 2"
                    />
                  </Grid>
                </>
              ) : values.new_question_type === 'multiple' ? (
                <>
                  {[...Array(6)].map((x, i) => (
                    <Grid item xs={4} key={`multiple-option-${i}`}>
                      <Field
                        fullWidth
                        required={i < 3}
                        name={`multiple_option_${i + 1}`}
                        component={TextField}
                        type="text"
                        label={`Option ${i + 1} ${i > 2 ? `(optional)` : ''}`}
                      />
                    </Grid>
                  ))}
                </>
              ) : (
                <></>
              )}
              <Grid item style={{ marginTop: 16 }} justifyContent="flex-end" container>
                <Button variant="contained" color="secondary" type="submit" disabled={submitting}>
                  Add Question
                </Button>
              </Grid>
            </Grid>
          </Card>
        </form>
      )}
    />
  );
}
