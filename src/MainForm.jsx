import React, { useCallback } from 'react';
import { Form } from 'react-final-form';
import { Paper, Grid, Button } from '@material-ui/core';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import QuestionRow from './QuestionRow.jsx';
import update from 'immutability-helper';

const onSubmit = async (values) => {
  const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};

export default function MainForm({ questions, setQuestions, editFormMode }) {
  const moveQuestion = useCallback((dragIndex, hoverIndex) => {
    setQuestions((questions) =>
      update(questions, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, questions[dragIndex]],
        ],
      }),
    );
  }, []);

  const renderQuestion = useCallback(({ index, moveQuestion, question, questions, editFormMode }) => {
    return (
      <QuestionRow
        index={index}
        moveQuestion={moveQuestion}
        question={question}
        questions={questions}
        editFormMode={editFormMode}
      />
    );
  }, []);

  return (
    <>
      <Form
        onSubmit={onSubmit}
        initialValues={{}}
        render={({ handleSubmit, form, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <DndProvider backend={HTML5Backend}>
                <Grid container spacing={2} alignItems="flex-end">
                  {questions.map((question, index) =>
                    renderQuestion({ question, questions, editFormMode, moveQuestion, index }),
                  )}
                  <>
                    <Grid item style={{ marginTop: 16 }} justifyContent="space-between" container>
                      <Button
                        type="button"
                        variant="contained"
                        onClick={() => form.reset()}
                        disabled={submitting || pristine}
                      >
                        Reset
                      </Button>
                      <Button variant="contained" color="primary" type="submit" disabled={submitting}>
                        Submit
                      </Button>
                    </Grid>
                  </>
                </Grid>
              </DndProvider>
            </Paper>
            {/* {editFormMode && <pre>{JSON.stringify(values, 0, 2)}</pre>} */}
          </form>
        )}
      />
    </>
  );
}
