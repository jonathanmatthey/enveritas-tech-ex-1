import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Grid, Button, CssBaseline } from '@material-ui/core';

import MainForm from './MainForm';
import AddQuestionForm from './AddQuestionForm';

function App() {
  const [editFormMode, setEditFormMode] = useState(false);
  const [questions, setQuestions] = useState([{ type: 'text', question: 'First Name' }]);

  return (
    <div style={{ padding: 16, margin: 'auto', maxWidth: 600 }}>
      <CssBaseline />
      <Grid container spacing={2} justifyContent="space-between" alignItems="center">
        <Grid item>
          <h2>Enveritas Tech Ex 1</h2>
        </Grid>
        <Grid item>
          <Button variant="text" size="small" color="secondary" onClick={() => setEditFormMode(!editFormMode)}>
            {editFormMode ? 'Save' : 'Edit'} Form
          </Button>
        </Grid>
      </Grid>
      <MainForm questions={questions} setQuestions={setQuestions} editFormMode={editFormMode} />
      {editFormMode && <AddQuestionForm questions={questions} setQuestions={setQuestions} />}
    </div>
  );
}

ReactDOM.render(<App />, document.querySelector('#root'));
