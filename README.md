# Enveritas Tech Ex 1

## Instructions

clone repo locally ... then

```
yarn
yarn start
```

open browser at localhost:3000

## Demo

<img src="public/enveritas-demo.gif" alt="demo" width="500"/>

demo:

https://jmp.sh/CkmPqcQ
